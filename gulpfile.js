var gulp = require('gulp');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var path = require('path');
var sass = require('gulp-sass');
var ftp = require('vinyl-ftp');
var notify = require("gulp-notify");
var secrets = require('./secrets');
var copy = require('gulp-copy');

var phpFiles = '**/*.php';
var cssPath = {
	adm:'assets/styles/adm/css',
	cli:'assets/styles/cli/css'
};
var cssFiles = {
	adm:'assets/styles/adm/css/**/*.css',
	cli:'assets/styles/cli/css/**/*.css'
};

var scssFiles = {
	adm:'assets/styles/adm/scss/**/*.scss',
	cli:'assets/styles/cli/scss/**/*.scss'
};
var jsFiles = {
	adm:'assets/js/adm/**/*.js',
	cli:'assets/js/cli/**/*.js'
}


var distPath = 'assets/dist';
var deployPath = secrets.path;
var connect = ftp.create({
  host: secrets.host,
  user: secrets.user,
  password: secrets.password,
  parallel: 10,
  log: gutil.log
});
var globs = [
  'index.php',
  'assets/**/*',
  'templates/**/*'
];

var deployLive = 0;
var prod = 0;
var onError = function (err) {
  gutil.beep();
  console.log(err);
};

function js(foo) {
  return gulp
  .src(jsFiles[foo])
  .pipe(plumber({
    errorHandler: onError
  }))
  .pipe(concat('dist'))
  .pipe(rename(foo + '.inv.js'))
	//.pipe(uglify())
  .pipe(gulp.dest(distPath));
}

function scss(foo) {
  return gulp
  .src(scssFiles[foo])
  .pipe(plumber({
    errorHandler: onError
  }))
  .pipe(sass())
  .pipe(gulp.dest(cssPath[foo]));
}

function css(foo) {
  return gulp
  .src(cssFiles[foo])
  .pipe(concat('dist'))
  .pipe(rename(foo + '.inv.css'))
  //.pipe(minifyCSS())
  .pipe(gulp.dest(distPath));
}
function deploy() {
  if (deployLive) {
    return gulp
    .src(
      globs, 
      {
        base:'.', 
        buffer:false
      }
    )
    .pipe(connect.newer(deployPath)) 
    .pipe(connect.dest(deployPath))
    .pipe(notify("Files deployed to " + deployPath));
    } else {
    return;
  }
}

gulp.task('default',function(){
	// SCSS
  gulp.watch(scssFiles['adm'], function() {
    console.log('ADM SASS files updated');
    return scss('adm');
  });
  gulp.watch(scssFiles['cli'], function() {
    console.log('CLI SASS files updated');
    return scss('cli');
  });
	// CSS
  gulp.watch(cssFiles['adm'], function() {
    console.log('ADM CSS files updated');
    return css('adm');
  });
	gulp.watch(cssFiles['cli'], function() {
    console.log('CLI CSS files updated');
    return css('cli');
  });
	// JS
  gulp.watch(jsFiles['adm'], function() {
    console.log('ADM JS Admin files updated');
    return js('adm');
  });
  gulp.watch(jsFiles['cli'], function() {
    console.log('CLI JS Client files updated');
    return js('cli');
  });
	// PHP
  gulp.watch(phpFiles, function(){
    console.log('PHP files updated');
    return;
  });
});
gulp.task('prod',function(){
  var prod = 1;
  return deploy();
});
gulp.task('deploy',function(){
  return deploy();
});