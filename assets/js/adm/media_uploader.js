jQuery(document).ready(function($){
	var media_uploader = null;

	function open_media_uploader_image() {
		media_uploader = wp.media({
			frame: "post", 
			state: "insert", 
			multiple: 0
		});

		media_uploader.on("insert", function(){
			var json = media_uploader.state().get("selection").first().toJSON();
			var thumb = json.sizes.thumbnail.url;

			$('#menu_icon_path').html(json.filename);
			$('#menu_icon').val(json.id);
			console.log(json);
		});

		media_uploader.open();

		$(".media-menu").find("a:contains('Gallery')").remove();
		$(".media-menu").find("a:contains('Playlist')").remove();
		$(".media-menu").find("a:contains('URL')").remove();
		$(".media-menu").find('.separator').remove();
	}

	$(document).on('click', '.upload', open_media_uploader_image);
});

