<?php
//Load your WordPress enviroment
$path = dirname(__FILE__);
$path = substr($path, 0, stripos($path, "wp-content") );
require( $path . "wp-load.php");

//TODO: filter and handle your $_GET params
$post_id = absint($_GET['post_id']);

$testing_img_url = wp_get_attachment_image_url(
  get_post_thumbnail_id($post->ID), 'full'
);

if(!$testing_img_url) {
	$testing_img_url = plugins_url() . '/inventorize/assets/img/ico.png';
}

$img = wp_get_image_editor( $testing_img_url );

if( ! is_wp_error( $img ) ) {
  header('Content-type:'.finfo_file ($testing_img_url));
  $img->resize(16, NULL, false);
  $img->set_quality(100);
  $img->stream();
}