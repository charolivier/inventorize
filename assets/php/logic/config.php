<?php
define('INV_PLUGIN_VERSION', '0.0.1');
define('INV_PLG_NAME', 'inventorize');

define('INV_PLG_PTH', plugins_url().'/'.INV_PLG_NAME.'/');
define('INV_ADM_PTH', INV_PLG_PTH.'assets/php/views/adm/');

define('INV_IMG_DIR', INV_PLG_DIR.'/assets/img/');
define('INV_DIS_DIR', INV_PLG_DIR.'/assets/dist/');
define('INV_PHP_DIR', INV_PLG_DIR.'/assets/php/');
define('INV_ADM_DIR', INV_PHP_DIR.'views/adm/');
define('INV_TMP_DIR', INV_PHP_DIR.'views/tmp/');
define('INV_DEBUG',1);