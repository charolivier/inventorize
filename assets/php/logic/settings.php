<?php
$inv = new inventorize();

register_activation_hook(INV_PLG_FILE, array($inv, 'plg_install'));
register_deactivation_hook(INV_PLG_FILE, array($inv, 'plg_uninstall'));

add_action('init', array($inv, 'start_session'));
add_action('init', array($inv, 'handle_request'));
add_action('init', array($inv, 'reg_taxonomy'));
add_action('init', array($inv, 'reg_post_type'));
add_action('admin_menu', array($inv, 'adm_menu'));
add_action('current_screen', array($inv, 'get_meta_box'));
add_action('after_setup_theme', array($inv, 'create_custom_image_size'));
add_action('wp_logout', array($inv,'end_session'));
add_action('add_meta_boxes', array($inv, 'reg_custom_meta_box'));
add_action('save_post', array($inv, 'upd_custom_field'));
add_action('wp_ajax_inv_ajax', array($inv, 'inv_ajax'));
?>