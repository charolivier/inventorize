<?php
class inventorize {
	var $pre;
	var $msg;
	var $db;
	var $req;
	var $tab;
	var $post;
	var $tab_post_type;
	var $tab_taxonomy;
	var $tab_meta_box;
	var $tab_cus_field;
	var $post_types;
	var $taxonomies;
	var $url;
	var $page;
	var $screen;
	var $note;

	function __construct() {
		global $wpdb, $wp_rewrite, $post;
		$this->db = $wpdb;
		$this->post = $post;
		$this->rewrite = $wp_rewrite;
		$this->req = $_REQUEST;
		$this->pre = 'inventorize';
		$this->tab = $this->db->prefix.INV_PLG_NAME;
		$this->tab_post_type = $this->tab.'_post_type';
		$this->tab_taxonomy = $this->tab.'_taxonomy';
		$this->tab_meta_box = $this->tab.'_meta_box';
		$this->tab_cus_field = $this->tab.'_cus_field';
	}

	// PLUGIN INSTALL
	function plugin_install() {
		
	}
	function plugin_uninstall() {
	 $this->del_table($this->tab_post_type);
	 $this->del_table($this->tab_taxonomy);
	}

	// REQ HANDLER
	function handle_request() {
		// STYLES AND SCRIPTS
		$this->enqueue_styles_and_scripts();

		// CREATE PLG TABLES
		$arr = array(
			"CREATE TABLE IF NOT EXISTS $this->tab_taxonomy (".$this->col_taxonomy().")",
			"CREATE TABLE IF NOT EXISTS $this->tab_post_type (".$this->col_post_type().")",
			"CREATE TABLE IF NOT EXISTS $this->tab_meta_box (".$this->col_custom_meta_box().")",
			"CREATE TABLE IF NOT EXISTS $this->tab_cus_field (".$this->col_custom_field().")"
		);
		foreach($arr as $k=>$v) $this->new_table($v);

		if(is_admin()) {
			if(isset($this->req['page']) && preg_match('/'.INV_PLG_NAME.'/i', $this->req['page'])) {
				$this->url = admin_url() . 'admin.php?page=' . $this->req['page'];

				// HANDLE POST TYPE PAGE 
				if($this->req['page']===$this->pre.'-post-type') {
					if(isset($this->req['action'])) {
						if(in_array($this->req['action'], array('create','edit'))) {
							// CREATE || EDIT
							$this->upd_post_type($this->req);
						} elseif($this->req['action']==='delete') {
							// DELETE
							$this->del_post_type($this->req);
						}
					}
				}

				// HANDLE TAXONOMY PAGE 
				if($this->req['page']===$this->pre.'-taxonomy') {
					if(isset($this->req['action'])) {
						if(in_array($this->req['action'], array('create','edit'))) {
							// CREATE || EDIT
							$this->upd_taxonomy($this->req);
						} elseif($this->req['action']==='delete') {
							// DELETE
							$this->del_taxonomy($this->req);
						}
					}
				}

				// HANDLE TAXONOMY PAGE 
				if($this->req['page']===$this->pre.'-meta-box') {
					if(isset($this->req['action'])) {
						if(in_array($this->req['action'], array('create','edit'))) {
							// CREATE || EDIT
							$this->upd_custom_meta_box($this->req);
						} elseif($this->req['action']==='delete') {
							// DELETE
							$this->del_custom_meta_box($this->req);
						}
					}
				}
			}
		}
		
		// DELECT TEMPORARY CUSTOM FIELDS
		$this->del_temp_custom_field();
	}

	// METABOXES
	function get_meta_box() {
		if(is_admin()) {
			if(isset($this->req['page']) && in_array($this->req['page'], array(
				 $this->pre.'-post-type'
				,$this->pre.'-taxonomy'
				,$this->pre.'-meta-box'
			))) {
				if(isset($this->req['action'])) {
					add_screen_option('layout_columns', array('max' => 2, 'default' => 2));
				}

				$this->screen = get_current_screen();

				add_action('add_meta_boxes_'.$this->screen->id, array($this, 'add_meta_box'));

				add_action('load-'.$this->screen->id, array($this, 'put_meta_box'), 9);
			}
		}
	}
	function put_meta_box() {
		do_action('add_meta_boxes_'.$this->screen->id, null);

		do_action('add_meta_boxes', $this->screen->id, null);
	}
	function add_meta_box() {
		if(isset($this->req['action']) && isset($this->req['page'])) {
			if(in_array($this->req['action'], array('create', 'edit'))) {
				if($this->req['page'] === $this->pre . '-post-type') {
					$list = array(
						array(
							'name'=>'Publish',
							'slug'=>'submitdiv',
							'type'=>'side',
							'page'=>'post-type'
						),
						array(
							'name'=>'Taxonomy',
							'slug'=>'taxonomy',
							'type'=>'normal',
							'page'=>'post-type'
						),
						array(
							'name'=>'Support',
							'slug'=>'support',
							'type'=>'normal',
							'page'=>'post-type'
						),
						array(
							'name'=>'Options',
							'slug'=>'options',
							'type'=>'normal',
							'page'=>'post-type'
						),
					);
				}
				if($this->req['page'] === $this->pre . '-taxonomy') {
					$list = array(
						array(
							'name'=>'Publish',
							'slug'=>'submitdiv',
							'type'=>'side',
							'page'=>'taxonomy'
						),
						array(
							'name'=>'Options',
							'slug'=>'options',
							'type'=>'normal',
							'page'=>'taxonomy'
						),
					);
				}
				if($this->req['page'] === $this->pre . '-meta-box') {
					$list = array(
						array(
							'name'=>'Publish',
							'slug'=>'submitdiv',
							'type'=>'side',
							'page'=>'meta-box'
						),
						array(
							'name'=>'Screen',
							'slug'=>'screen',
							'type'=>'normal',
							'page'=>'meta-box'
						),
						array(
							'name'=>'Options',
							'slug'=>'options',
							'type'=>'normal',
							'page'=>'meta-box'
						),
					);
				}
			}
		}

		if(isset($list)) {
			foreach($list as $l) {
				add_meta_box($l['slug'], $l['name'], array($this, 'tmp_box'), $this->screen->id, $l['type'], 'high', array('tmp'=>$l['slug'],'pge'=>$l['page']));
			}
		}
	}

	// INV SETTINGS
	function plg_settings() {
		if(is_admin() && preg_match('/'.INV_PLG_NAME.'/i', $this->req['page'])) {
			if(!current_user_can('manage_options')) wp_die(__('You do not have sufficient permissions to access this page.'));
		}

		if(is_admin()) $this->tmp_adm();
	}

	// INV ADMIN MENU
	function adm_menu() {
		$p = array(
			'page_title' => 'Inventorize',
			'menu_title' => 'Inventorize',
			'capability' => 'manage_options',
			'menu_slug' => $this->pre . '-dashboard',
			'function' => array($this, 'plg_settings'),
			'icon' => 'dashicons-warning',
			'position' => NULL,
		);

		add_menu_page($p['page_title'], $p['menu_title'], $p['capability'], $p['menu_slug'], $p['function'], $p['icon'], $p['position']);

		$children = array(
			array(
				'page_title' => 'Dashboard',
				'menu_title' => 'Dashboard',
				'capability' => 'manage_options',
				'menu_slug' => $this->pre . '-dashboard',
				'function' => array($this, 'plg_settings'),
			),
			array(
				'page_title' => 'Inventories',
				'menu_title' => 'Inventories',
				'capability' => 'manage_options',
				'menu_slug' => $this->pre . '-post-type',
				'function' => array($this, 'plg_settings'),
			),
			array(
				'page_title' => 'Taxonomies',
				'menu_title' => 'Taxonomies',
				'capability' => 'manage_options',
				'menu_slug' => $this->pre . '-taxonomy',
				'function' => array($this, 'plg_settings'),
			),
			array(
				'page_title' => 'Meta Boxes',
				'menu_title' => 'Meta Boxes',
				'capability' => 'manage_options',
				'menu_slug' => $this->pre . '-meta-box',
				'function' => array($this, 'plg_settings'),
			),
		);

		foreach($children as $c) {
			add_submenu_page($p['menu_slug'], $c['page_title'], $c['menu_title'], $c['capability'], $c['menu_slug'], $c['function']);
		}
	}

	// TEMPLATES
	function tmp_adm() {
		$tmp = INV_ADM_DIR . str_replace($this->pre.'-', '', $this->req['page']) . '.php';

		$this->return_file($tmp);
	}
	function tmp_box($post, $callback_args) {
		$tmp = $callback_args['args']['tmp'];

		$pge = $callback_args['args']['pge'];

		if(isset($pge)) {
			if($pge === 'post-type') $inv_post_type = $this->get_post_type();

			if($pge === 'taxonomy') $inv_taxonomy = $this->get_taxonomy();

			if($pge === 'meta-box') $inv_meta_box = $this->get_custom_meta_box();
		}

		include_once(INV_ADM_DIR.'meta/'.$pge.'/'.$tmp.'.php');
	}
	function return_file($filePath, $data=0) {
		if(is_file($filePath)) {
			ob_start();

			include($filePath);

			return ob_get_contents();

			ob_end_clean();
		} else {
			trigger_error($filePath.' file not found');
		}
	}

	// SCRIPTS-STYLES
	function enqueue_styles_and_scripts() {
		if(is_admin()) {
			wp_enqueue_style('adm_menu_style', plugins_url(INV_PLG_NAME.'/assets/dist/adm.inv.css'));

	    wp_localize_script('jquery', 'php_vars', array(
	    		'admin' => admin_url('admin-ajax.php'),
	      	'ajax_nonce' => wp_create_nonce(INV_PLG_NAME . '_ajax_nonce')
	    	)
			);
			
			if(isset($this->req['page'])) {
				if(preg_match('/'.INV_PLG_NAME.'/i', $this->req['page'])){
					wp_enqueue_script('adm_script', plugins_url(INV_PLG_NAME.'/assets/dist/adm.inv.js'), array('jquery'));
					wp_enqueue_script('postbox');
				}

				if($this->req['page']===$this->pre.'-post-type') {
					add_filter('upload_mimes', array($this, 'filter_media_upload_myme_types'));

					wp_enqueue_media();
				}

				if($this->req['page']===$this->pre.'-meta-box') {
					wp_enqueue_script('thickbox');

					wp_enqueue_style('thickbox');
				}

				if(in_array($this->req['page'],array($this->pre.'-post-type', $this->pre.'-meta-box'))) {
					wp_enqueue_script('chosen_script', plugins_url(INV_PLG_NAME.'/assets/vendor/chosen/chosen.jquery.min.js'), array('jquery'));

					wp_enqueue_style('chosen_style', plugins_url(INV_PLG_NAME.'/assets/vendor/chosen/chosen.min.css'));
				}
			}
		}
	}

	// IMAGE UPLOADER SETTINGS
	function create_custom_image_size() {
		add_theme_support('post-thumbnails');

		add_image_size('favicon', 16, 16, false);
	}
	function filter_media_upload_myme_types($mime_types) {
		$mime_types = array(
	    'jpg|jpeg|jpe' => 'image/jpeg',
	    'png' => 'image/png',
    );

		return $mime_types;
	}

	// POST TYPE
	function col_post_type(){
	$str = "
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NULL,
	name_plural VARCHAR(255) NULL,
	slug VARCHAR(255) NULL,
	description VARCHAR(50000) NULL,
	public BOOLEAN NULL,
	publicly_queryable BOOLEAN NULL,
	show_ui BOOLEAN NULL,
	show_in_nav_menus BOOLEAN NULL,
	show_in_menu BOOLEAN NULL,
	show_in_admin_bar BOOLEAN NULL,
	menu_position VARCHAR(3) NULL,
	hierarchical BOOLEAN NULL,
	menu_icon VARCHAR(255) NULL,
	supports VARCHAR(50000) NULL,
	taxonomies VARCHAR(50000) NULL,
	has_archive BOOLEAN NULL,
	status VARCHAR(255) NULL,
	PRIMARY KEY  (id)
	";

	return $str;
	}
	function reg_post_type() {
		$this->post_types = $this->get_row($this->tab_post_type);

		foreach($this->post_types as $v) {
			$labels = array(
				 'name' => __($v->name)
				,'singular_name' => __($v->name)
				,'add_new_item' => __('Add New' . $v->name)
				,'edit_item' => __('Edit ' . $v->name)
				,'new_item' => __('New ' . $v->name)
				,'view_item' => __('View ' . $v->name)
				,'view_items' => __('View' . $v->name_plural)
				,'search_items' => __('Search ' . $v->name_plural)
				,'not_found' => __('Nothing found')
				,'not_found_in_trash' => __('Nothing found in the Trash')
				,'all_items' => __('All ' . $v->name_plural)
				,'archives' => __('Archives')
				,'attributes' => __('Attributes')
				,'insert_into_item' => __('Insert')
				,'uploaded_to_this_item' => __('Upload')
			);

			$args = array(
				 'labels'=>$labels
				,'public'=>($v->public)?TRUE:FALSE
				,'publicly_queryable'=>($v->publicly_queryable)?TRUE:FALSE
				,'show_ui'=>($v->show_ui)?TRUE:FALSE
				,'show_in_nav_menus'=>($v->show_in_nav_menus)?TRUE:FALSE
				,'show_in_menu'=>($v->show_in_menu)?TRUE:FALSE
				,'show_in_admin_bar'=>($v->show_in_admin_bar)?TRUE:FALSE
				,'menu_position'=>(int)$v->menu_position
				,'hierarchical'=>($v->hierarchical)?TRUE:FALSE
				,'has_archive'=>($v->has_archive)?TRUE:FALSE
				,'menu_icon'=>wp_get_attachment_image_src($v->menu_icon,'favicon')[0]
				,'supports'=>$this->json_validator($v->supports)?json_decode($v->supports):0
				,'taxonomies'=>$this->json_validator($v->taxonomies)?json_decode($v->taxonomies):0
			);

			register_post_type($v->slug, $args);
		}
	}
	function get_post_type() {
		if($this->req['action'] === 'edit') {
			$res = $this->get_row($this->tab_post_type, array('id'=>$this->req['id']));

			$res = $res[0];
		}

		if(!$this->get_error() && $this->req['action'] === 'create' && isset($this->req['update'])) {
			$arr = $this->get_row($this->tab_post_type);

			$res = end($arr);
		}

		if(isset($res) && is_object($res)) {
			return $res;
		} else {
			return (object)array(
				 'name'=>''
				,'name_plural'=>''
				,'slug'=>''
				,'description'=>''
				,'public'=>1
				,'publicly_queryable'=>1
				,'show_ui'=>1
				,'show_in_nav_menus'=>1
				,'show_in_menu'=>1
				,'show_in_admin_bar'=>1
				,'menu_position'=>5
				,'hierarchical'=>1
				,'has_archive'=>1
				,'menu_icon'=>0
				,'taxonomies'=>json_encode(array('category','post_tag'))
				,'supports'=>json_encode(array('title','editor','thumbnail'))
				,'status' => __('published')
			);
		}
	}
	function upd_post_type($req) {
		$id = isset($req['id']) ? $req['id'] : 0;

		if(isset($req['update'])) {
			$required = $this->check_required($req, array('name','name_plural'));

			if(!$required) {
				$nonce = array('action'=>'inv_post_type_'.$this->req['action'],'field'=>'inv_post_type_'.$this->req['action'].'_nonce_field');

				// SLUGIFY
				$slug = ($req['slug']) ? $req['slug'] : $req['name'];
				$slug = sanitize_title_with_dashes($slug);
				$slug = $this->no_duplicate($slug, $this->tab_post_type, $id);

				// CHECK WP SECURE NONCE
				if($this->check_nonce($req, $nonce['action'], $nonce['field'])) {
					$data = array(
						 'name'=>sanitize_text_field($req['name'])
						,'name_plural'=>sanitize_text_field($req['name_plural'])
						,'slug'=>$slug
						,'description'=>$req['description'] ? sanitize_textarea_field($req['description']) : ''
						,'public'=>isset($req['public']) ? 1 : 0
						,'publicly_queryable'=>isset($req['publicly_queryable']) ? 1 : 0
						,'show_ui'=>isset($req['show_ui']) ? 1 : 0
						,'show_in_nav_menus'=>isset($req['show_in_nav_menus']) ? 1 : 0
						,'show_in_menu'=>isset($req['show_in_menu']) ? 1 : 0
						,'show_in_admin_bar'=>isset($req['show_in_admin_bar']) ? 1 : 0
						,'menu_position'=>isset($req['menu_position'])? (int) $req['menu_position'] : 5
						,'hierarchical'=>isset($req['hierarchical']) ? 1 : 0
						,'has_archive'=>isset($req['has_archive']) ? 1 : 0
						,'menu_icon'=>sanitize_text_field($req['menu_icon'])
						,'taxonomies'=>(isset($req['taxonomies'])) ? json_encode($this->sanitize_array($req['taxonomies'])) : json_encode(array())
						,'supports'=>json_encode($this->sanitize_array($req['supports']))
						,'status'=>sanitize_text_field($req['status'])
					);

					if($this->req['action']==='edit') {
						// UPDATE POST-TYPES TABLE
						$this->upd_row($this->tab_post_type, $data, array('id'=>$id));

						// UPDATE TAX TABLE
						$this->upd_tax_type($data);

						$this->admin_notify('success', 'edit', 'Inventory <strong>'.$data['name'].'</strong> updated.');

						header('Location: '.$this->url.'&action=edit&msg&id='.$id);
					}

					if($this->req['action']==='create') {
						// UPDATE POST-TYPES TABLE
						$this->put_row($this->tab_post_type, $data);

						// UPDATE TAX TABLE
						$this->upd_tax_type($data);

						$arr = $this->get_row($this->tab_post_type);

						$this->admin_notify('success', 'create', 'New Inventory <strong>'.$data['name'].'</strong> created.');

						header('Location: '.$this->url.'&action=edit&response&id='.end($arr)->id);
					}
				}
			} 
			else {
				header('Location: ' . $this->url . '&action=' . $this->req['action'] . '&msg' . (($id) ? '&id=' . $id : ''));

				$this->admin_notify('error', $this->req['action'], $required);
			}
		}
	}
	function del_post_type($req) {
		if(isset($req['id'])) {
			if(is_array($req['id'])) {
				foreach($req['id'] as $k=>$v) {
					$this->del_post_type_tax(array('id'=>$v));

					$this->del_row($this->tab_post_type, array('id'=>$v));
				}
			} else {
			$this->del_post_type_tax(array('id'=>$req['id']));

			$this->del_row($this->tab_post_type, array('id'=>$req['id']));
			}

			header('Location: ' . $this->url . '&msg');

			$this->admin_notify('success', 'edit', 'Inventory deleted.');
		} else {
			header('Location: ' . $this->url . '&msg');

			$this->admin_notify('error', 'delete', 'Nothing to delete');
		}
	}
	function del_post_type_tax($req) {
		$del_tax = $this->get_row($this->tab_taxonomy, $req);
		$del_tax = $del_tax[0];

		$post_types = $this->get_row($this->tab_post_type);

		if ($del_tax) {
			foreach($post_types as $k=>$v){
				$taxonomies = json_decode($v->taxonomies);

				$upd = 0;

				if(in_array($del_tax->slug, $taxonomies)) {
					$taxonomies = array_diff($taxonomies, array($del_tax->slug));

					$upd = 1;
				}

				if($upd) $this->upd_row($this->tab_post_type, array('taxonomies'=>json_encode($taxonomies)), array('id'=>$v->id));
			}
		}
	}

	// TAXONOMY
	function col_taxonomy() {
		$str = "
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		name VARCHAR(255) NULL,
		name_plural VARCHAR(255) NULL,
		slug VARCHAR(255) NULL,
		description VARCHAR(50000) NULL,
		types VARCHAR(50000) NULL,
		public BOOLEAN NULL,
		publicly_queryable BOOLEAN NULL,
		show_ui BOOLEAN NULL,
		show_in_menu BOOLEAN NULL,
		show_in_nav_menus BOOLEAN NULL,
		show_in_rest BOOLEAN NULL,
		show_in_quick_edit BOOLEAN NULL,
		show_admin_column BOOLEAN NULL,
		hierarchical BOOLEAN NULL,
		rewrite VARCHAR(50000) NULL,
		sort BOOLEAN NULL,
		status VARCHAR(255) NULL,
		PRIMARY KEY  (id)
		";

		return $str;
	}
	function reg_taxonomy() {
		$this->taxonomies = $this->get_row($this->tab_taxonomy);

		foreach($this->taxonomies as $k=>$v) {
			$labels = array(
			 'name' => __($v->name)
			,'singular_name' => __($v->name)
			,'all_items' => __('All ' . $v->name_plural)
			,'edit_item' => __('Edit ' . $v->name)
			,'view_item' => __('View ' . $v->name)
			,'update_item' => __('Update ' . $v->name)
			,'add_new_item' => __('Add New' . $v->name)
			,'new_item_name' => __('New ' . $v->name . 'Name')
			,'parent_item' => __('Parent ' . $v->name)
			,'parent_item_colon' => __('Parent ' . $v->name .':')
			,'search_items' => __('Search ' . $v->name_plural)
			,'popular_items' => __('Popular ' . $v->name_plural)
			,'separate_items_with_commas' => __('Separate ' . $v->name_plural . ' with commas ')
			,'add_or_remove_items' => __('Add or remove ' . $v->name_plural)
			,'choose_from_most_used' => __('Choose from the most used ' . $v->name_plural)
			,'not_found' => __('Nothing found')
			);

			$args = array(
				 'labels' => $labels
				,'public' => ($v->public)?TRUE:FALSE
				,'publicly_queryable' => ($v->publicly_queryable)?TRUE:FALSE
				,'show_ui' => ($v->show_ui)?TRUE:FALSE
				,'show_in_menu' => ($v->show_in_menu)?TRUE:FALSE
				,'show_in_nav_menus' => ($v->show_in_nav_menus)?TRUE:FALSE
				,'show_in_rest' => ($v->show_in_rest)?TRUE:FALSE
				,'show_in_quick_edit' => ($v->show_in_quick_edit)?TRUE:FALSE
				,'show_admin_column' => ($v->show_admin_column)?TRUE:FALSE
				,'hierarchical' => ($v->hierarchical)?TRUE:FALSE
				,'description' => ($v->description)?$v->description:''
				,'rewrite' => $this->json_validator($v->rewrite)?json_decode($v->rewrite):0
				,'sort' => ($v->sort)?TRUE:FALSE
			);

			$object_type = json_decode($v->types);

			if($object_type) {
				foreach($object_type as $ko=>$vo) {
					register_taxonomy($v->slug, $vo, $args);
				}
			}
		}
	}
	function get_taxonomy() {
		if(isset($this->req['action'])) {
			if($this->req['action'] === 'edit') {
				$res = $this->get_row($this->tab_taxonomy, array('id'=>$this->req['id']));
				$res = $res[0];
			}

			if(!$this->get_error() && $this->req['action'] === 'create' && isset($this->req['update'])) {
				$arr = $this->get_row($this->tab_taxonomy);

				$res = end($arr);
			}
		}

		if(isset($res) && is_object($res)) {
			return $res;
		} else {
			return (object) array(
				 'name'=>''
				,'name_plural'=>''
				,'slug'=>''
				,'description'=>''
				,'public'=>1
				,'publicly_queryable'=>1
				,'show_ui'=>1
				,'show_in_menu'=>1
				,'show_in_nav_menus'=>1
				,'show_in_rest'=>1
				,'show_in_quick_edit'=>1
				,'show_admin_column'=>1
				,'hierarchical'=>1
				,'rewrite'=>0
				,'sort'=>1
				,'types'=>array()
				,'status' => __('published')
			);
		}
	}
	function upd_taxonomy($req) {
		$id = isset($req['id']) ? $req['id'] : 0;

		if(isset($req['update'])) {
			$required = $this->check_required($req, array('name','name_plural'));

			if(!$required) {
				$nonce = array('action'=>'inv_taxonomy_'.$this->req['action'],'field'=>'inv_taxonomy_'.$this->req['action'].'_nonce_field');

				// SLUGIFY
				$slug = ($req['slug']) ? $req['slug'] : $req['name'];
				$slug = sanitize_title_with_dashes($slug);
				$slug = $this->no_duplicate($slug, $this->tab_taxonomy, $id);

				// CHECK WP SECURE NONCE
				if($this->check_nonce($req, $nonce['action'], $nonce['field'])) {
					$data = array(
						 'name'=>sanitize_text_field($req['name'])
						,'name_plural'=>sanitize_text_field($req['name_plural'])
						,'slug'=>$slug
						,'description'=>$req['description'] ? sanitize_textarea_field($req['description']) : ''
						,'public'=>isset($req['public']) ? 1 : 0
						,'publicly_queryable'=>isset($req['publicly_queryable']) ? 1 : 0
						,'show_ui'=>isset($req['show_ui']) ? 1 : 0
						,'show_in_menu'=>isset($req['show_in_menu']) ? 1 : 0
						,'show_in_nav_menus'=>isset($req['show_in_nav_menus']) ? 1 : 0
						,'show_in_rest'=>isset($req['show_in_rest']) ? 1 : 0
						,'show_in_quick_edit'=>isset($req['show_in_quick_edit']) ? 1 : 0
						,'show_admin_column'=>isset($req['show_admin_column']) ? 1 : 0
						,'hierarchical'=>isset($req['hierarchical']) ? 1 : 0
						,'rewrite'=>isset($req['rewrite_enable'])?json_encode($this->sanitize_array(array('slug'=>$req['rewrite_slug']))):0
						,'sort'=>isset($req['sort']) ? 1 : 0
						,'types'=>isset($req['types']) ? json_encode($req['types']) : json_encode(array())
					);

					if($this->req['action']==='edit') {
						$this->upd_row($this->tab_taxonomy, $data, array('id'=>$req['id']));

						header('Location: '.$this->url.'&action=edit&msg&id='.$id);

						$this->admin_notify('success', 'edit', 'Taxonomy <strong>'.$data['name'].'</strong> updated.');

						$this->rewrite->flush_rules();
					}

					if($this->req['action']==='create') {
						$this->put_row($this->tab_taxonomy, $data);

						$arr = $this->get_row($this->tab_taxonomy);

						header('Location: '.$this->url.'&action=edit&msg&id='.end($arr)->id);

						$this->admin_notify('success', 'create', 'New Taxonomy <strong>'.$data['name'].'</strong> created.');

						$this->rewrite->flush_rules();
					}
				}
			} 
			else {
				header('Location: ' . $this->url . '&action=' . $this->req['action'] . '&msg' . (($id) ? '&id=' . $id : ''));

				$this->admin_notify('error', $this->req['action'], $required);
			}
		}
	}
	function del_taxonomy($req) {
		if(isset($req['id'])) {
			if(is_array($req['id'])) {
				foreach($req['id'] as $k=>$v) {
					$this->del_post_type_tax(array('id'=>$v));

					$this->del_row($this->tab_taxonomy, array('id'=>$v));
				}
			} else {
				$this->del_post_type_tax(array('id'=>$req['id']));

				$this->del_row($this->tab_taxonomy, array('id'=>$req['id']));
			}

			header('Location: ' . $this->url . '&msg');

			$this->admin_notify('success', 'delete', 'Taxonomy deleted.');
		} else {
			header('Location: ' . $this->url . '&msg');

			$this->admin_notify('error', 'delete', 'Nothing to delete');
		}
	}
	function upd_tax_type($req) {
		$tax = $this->get_row($this->tab_taxonomy);

		foreach($tax as $k=>$v){
			$types = json_decode($v->types);

			$upd = 0;

			if(in_array($v->slug, json_decode($req['taxonomies']))) {
				// ADD TYPE FROM TAX
				if(!in_array($req['slug'], $types)) {
					$types[] = $req['slug'];

					$upd = 1;
				}
			} else {
				// REMOVE TYPE FROM TAX
				if(in_array($req['slug'], $types)) {
					$types = array_diff($types, array($req['slug']));

					$upd = 1;
				}
			}

			if($upd) $this->upd_row($this->tab_taxonomy, array('types'=>json_encode($types)), array('slug'=>$v->slug));
		}
	}
	function del_tax_type($req) {
		$post_type = $this->get_row($this->tab_post_type, $req);
		$post_type = $post_type[0];

		$tax = $this->get_row($this->tab_taxonomy);

		if ($post_type) {
			foreach($tax as $k=>$v){
				$types = json_decode($v->types);

				$upd = 0;

				if(in_array($post_type->slug, $types)) {
					$types = array_diff($types, array($post_type->slug));

					$upd = 1;
				}

				if($upd) $this->upd_row($this->tab_taxonomy, array('types'=>json_encode($types)), array('id'=>$v->id));
			}
		}
	}

	// CUSTOM META BOX
	function col_custom_meta_box() {
		$str = "
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		name VARCHAR(255) NULL,
		slug VARCHAR(255) NULL,
		description VARCHAR(50000) NULL,
		context VARCHAR(255) NULL,
		priority VARCHAR(255) NULL,
		screen VARCHAR(5000) NULL,
		callback_args VARCHAR(5000) NULL,
		status VARCHAR(255) NULL,
		PRIMARY KEY  (id)
		";

		return $str;
	}
	function reg_custom_meta_box() {
		$this->custom_meta_box = $this->get_row($this->tab_meta_box);

		foreach($this->custom_meta_box as $k=>$v) {
			$args = array(
				 'name' => ($v->name) ? $v->name : ''
				,'slug' => ($v->slug) ? $this->pre.'_'.$v->slug : ''
				,'description' => ($v->description) ? $v->description : ''
				,'context' => ($v->context) ? $v->context : ''
				,'priority' => ($v->priority) ? $v->priority : ''
				,'screen' => ($v->screen) ? $v->screen : ''
				,'callback_args' => ($v->callback_args) ? $v->callback_args : 0
				,'status' => ($v->status) ? $v->status : 0
			);

			if ($args['status'] && $args['status']==='published' && !empty($args['screen'])){
				add_meta_box(
					$args['slug'], 
					$args['name'], 
					array($this, 'reg_custom_field'), 
					json_decode($args['screen']),
					$args['context'],
					$args['priority'],
					$args['callback_args']
				);
			}
		}
	}
	function get_custom_meta_box() {
		if(isset($this->req['action'])) {
			if($this->req['action'] === 'edit') {
				$res = $this->get_row($this->tab_meta_box, array('id'=>$this->req['id']));
				$res = $res[0];
			}

			if(!$this->get_error() && $this->req['action'] === 'create' && isset($this->req['update'])) {
				$arr = $this->get_row($this->tab_meta_box);

				$res = end($arr);
			}
		}

		if(isset($res) && is_object($res)) {
			return $res;
		} else {
			return (object) array(
				 'name'=>''
				,'slug'=>''
				,'description'=>''
				,'context'=>'normal'
				,'priority'=>'priority'
				,'screen'=>'[]'
				,'callback_args' => '[]'
				,'status' => __('published')
			);
		}
	}
	function upd_custom_meta_box($req) {
		$id = isset($req['id']) ? $req['id'] : 0;

		if(isset($req['update'])) {
			$required = $this->check_required($req, array('name'));

			if(!$required) {
				$nonce = array('action'=>'inv_meta_box_'.$this->req['action'],'field'=>'inv_meta_box_'.$this->req['action'].'_nonce_field');

				// SLUGIFY
				$slug = ($req['slug']) ? $req['slug'] : $req['name'];
				$slug = sanitize_title_with_dashes($slug);
				$slug = $this->no_duplicate($slug, $this->tab_meta_box, $id);

				// CHECK WP SECURE NONCE
				if($this->check_nonce($req, $nonce['action'], $nonce['field'])) {
					$data = array(
						 'name'=>sanitize_text_field($req['name'])
						,'slug'=>$slug
						,'description' => (isset($req['description'])) ? sanitize_textarea_field($req['description']) : ''
						,'context' => (isset($req['context'])) ? sanitize_text_field($req['context']):''
						,'priority' => (isset($req['priority'])) ? sanitize_text_field($req['priority']):''
						,'screen' => (isset($req['screen'])) ? json_encode($this->sanitize_array($req['screen'])) : json_encode(array())
						,'callback_args' => (isset($req['args'])) ? json_encode($this->sanitize_array($req['args'])) : json_encode(array())
						,'status' => sanitize_text_field($req['status'])
					);
					
					if(in_array($this->req['action'], array('edit','create')) && isset($req['fields'])) {
						$field_ids = array();

						foreach($req['fields'] as $k=>$v) {
							$this->upd_custom_field_def($v);

							$field_ids[] = $v['id'];
						}
						
						$data['callback_args'] = json_encode($field_ids);
					}

					if($this->req['action']==='edit') {
						$this->upd_row($this->tab_meta_box, $data, array('id'=>$id));

						$this->admin_notify('success', 'edit', 'Inventory <strong>'.$data['name'].'</strong> updated.');

						header('Location: '.$this->url.'&action=edit&msg&id='.$id);
					}

					if($this->req['action']==='create') {
						$this->put_row($this->tab_meta_box, $data);

						$arr = $this->get_row($this->tab_meta_box);

						$this->admin_notify('success', 'create', 'New Inventory <strong>'.$data['name'].'</strong> created.');

						header('Location: '.$this->url.'&action=edit&response&id='.end($arr)->id);
					}
				}
			} 
			else {
				header('Location: ' . $this->url . '&action=' . $this->req['action'] . '&msg' . (($id) ? '&id=' . $id : ''));

				$this->admin_notify('error', $this->req['action'], $required);
			}
		}
	}
	function del_custom_meta_box($req) {
		if(isset($req['id'])) {
			if(is_array($req['id'])) {
				foreach($req['id'] as $k=>$v) {
					$this->del_row($this->tab_meta_box, array('id'=>$v));
				}
			} else {
				$this->del_row($this->tab_meta_box, array('id'=>$req['id']));
			}

			header('Location: ' . $this->url . '&msg');

			$this->admin_notify('success', 'edit', 'Metabox deleted.');
		} else {
			header('Location: ' . $this->url . '&msg');

			$this->admin_notify('error', 'delete', 'Nothing to delete');
		}
	}

	// CUSTOM FIELD
	function col_custom_field() {
		$str = "
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		name VARCHAR(255) NULL,
		slug VARCHAR(255) NULL,
		description VARCHAR(50000) NULL,
		type VARCHAR(255) NULL,
		temp BOOLEAN NULL,
		PRIMARY KEY  (id)
		";

		return $str;
	}
	function reg_custom_field($post, $req) {
		foreach(json_decode($req['args']) as $k=>$v) {
			$data = $this->get_row($this->tab_cus_field, array('id'=>$v));

			$data = array(
				'post'=>$post,
				'field'=>$data[0]
			);

			$path = INV_ADM_DIR.'meta/cus-field/'.$data['field']->type.'.php';

			$this->return_file($path, $data);
		}
	}
	function upd_custom_field_def($req) {
		if(!$req) {
			$this->put_row($this->tab_cus_field, array('temp'=>1));
		} else {
			$this->upd_row($this->tab_cus_field, $req, array('id'=>$req['id']));
		}
	}
	function upd_custom_field($post_id) {
		foreach($this->get_row($this->tab_cus_field) as $k=>$v) {
			$slug = 'inv_' . $v->slug;

			if(isset($_POST[$slug])) update_post_meta($post_id, $slug, $_POST[$slug]);
		}		
	}
	function del_temp_custom_field(){
		$this->del_row($this->tab_cus_field, array('temp'=>1));
	}

	// SESSION
	function start_session() {
		if(!session_id()) session_start();
	}
	function end_session() {
		session_destroy ();
	}

	// TOOLBOX
	function new_table($req) {
		if(isset($req)) {
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

			$charset = $this->db->get_charset_collate();

			$sql = $req . " " . $charset . ";";

			dbDelta($sql);
		}
	}
	function del_table($tab) {
		$sql = "DROP TABLE IF EXISTS ".$tab.";";
		$this->db->query($sql);
		delete_option("my_plugin_db_version");
	}
	function get_table($tab) {
		return $this->$db->get_var("SELECT * FROM $tab");
	}

	function put_row($tab="", $arr) {
		$this->db->insert($tab, $arr);
	}
	function del_row($tab="", $arr) {
		$this->db->delete($tab, $arr);
	}
	function get_row($tab="", $arr=0) {
		if($arr) {
			$args = '';

			$search = 0;

			$query = "SELECT * FROM " . $tab;

			// SEARCH
			foreach($arr as $k=>$v) {
				if($k === 's') {
					$query .= " WHERE name LIKE '%".$v."%'";

					$search++;
				}
			}

			// WHERE
			if(!$search) {
				foreach($arr as $k=>$v) {
					if($k !== 's') {
						$args .= $k."='".$v."'";

						if($k+1 < count($arr)) $args .= " AND ";
					}
				}

				$query .= " WHERE " . $args;
			}
		}

		if(isset($args) || (isset($search) && $search)) {
			$res = $this->db->get_results($query);

			if($res) {
				return $res;
			} else {
				return 0;
			}
		} else {
			return($this->db->get_results("SELECT * FROM $tab"));
		}
	}
	function upd_row($tab="", $data, $arr) {
		$this->db->update($tab, $data, $arr);
	}

	function no_duplicate($slug, $table, $id=0, $count=0) {
		$res = $this->get_row($table, array('slug'=>$slug));

		$res = $res[0];

		if($res && $res->id !== $id) {
			$count++;

			$arr = explode('-', $slug);

			if($count>1) array_pop($arr);

			$slug = implode('-', $arr);

			return $this->no_duplicate($slug.'-'.$count, $table, $id, $count);
		}

		return $slug;
	}
	function admin_notify($status, $action, $msg) {
		$_SESSION['inv_notify'] = array('status'=>$status, 'action'=>$action, 'msg'=>$msg);
		// echo '<div class="notice notice-'.$state.' is-dismissible"><p>'.$msg.'</p></div>';
	}
	function check_required($req, $arr) {
		$missing = array();

		foreach($arr as $k=>$a) {
			if(!$req[$a] || $req[$a]=='') array_push($missing, $a);
		}

		if(!empty($missing)){
			$msg = 'Required entries are missing: ';

			foreach($missing as $k=>$m) {
				$msg .= $m;

				if($k!=(count($missing)-1)) $msg .= ', ';
			}

			return $msg;
		} else {
			return 0;
		}
	}
	function sanitize_array($array){
		foreach($array as $key=>$val) {
			$array[$key]=sanitize_text_field($val);
		}

		return $array;
	}
	function check_nonce($req, $nonce_action, $nonce_field) {
		if(!isset($req[$nonce_field]) || !wp_verify_nonce($req[$nonce_field], $nonce_action)) {
			return 0;
		} else {
			return 1;
		}
	}
	function get_error() {
		if(isset($_SESSION['inv_notify'])) {
			if(isset($_SESSION['inv_notify']['status']) && $_SESSION['inv_notify']['status'] === 'error') {
				return $_SESSION['inv_notify']['action'];
			}

			return;
		}
	}
	function json_validator($data=0) {
		if(!empty($data)) {
			@json_decode($data);

			return (json_last_error() === JSON_ERROR_NONE);
		}

		return false;
	}
	
	function inv_ajax() {
		require_once(INV_ADM_DIR . '/ajax.php');

		die();
	}
}
?>