<!--METABOX edit.php-->

<?php $inv_meta_box = $this->get_custom_meta_box(); ?>

<form id='inv-meta-box' action='<?php echo admin_url('admin.php'); ?>'>
	<input type='hidden' name='page' value='inventorize-meta-box'>

	<input type='hidden' name='update' value='1'>

	<?php if($error !== 'create' && ($action && $action === 'edit' || isset($this->req['update']))) { ?>
		<input type='hidden' name='action' value='edit'>

		<?php wp_nonce_field('inv_meta_box_edit', 'inv_meta_box_edit_nonce_field', 0); ?>

		<input type='hidden' name='id' value='<?php echo $inv_meta_box->id; ?>'>
	<?php } else { ?>
		<input type='hidden' name='action' value='create'>

		<?php wp_nonce_field('inv_meta_box_create','inv_meta_box_create_nonce_field'); ?>
	<?php } ?>

	<?php wp_nonce_field('meta-box-order', 'meta-box-order-nonce', false ); ?>
	
	<?php wp_nonce_field('inv_custom_field', 'inv_custom_field_nonce_field', 0); ?>

	<?php wp_nonce_field('closedpostboxes', 'closedpostboxesnonce', false ); ?>

	<div id='poststuff'>
		<div id='post-body' class='postbox-holder columns-<?php echo 1 == get_current_screen()->get_columns() ? '1' : '2'; ?>'>
			<div id='post-body-content'>
				<table class='widefat fixed striped'>
					<thead>
						<tr>
							<th>Name and description</th>
						</tr>
					</thead>

					<tbody>
						<tr class='tr_name'>
							<td>
								<div class='row'>
									<label for='name'><strong>Name</strong></label>
								</div>

								<div class='row'>
									<small>General name</small>
								</div>

								<div class='row'>
									<input type='text' name='name' style='width:100%' value='<?php echo $inv_meta_box->name; ?>' />
								</div>
							</td>
						</tr>

						<tr class='tr_slug'>
							<td>
								<div class='row'>
									<label for='slug'><strong>Slug</strong></label>
								</div>

								<div class='row'>
									<small>User friendly and URL valid name</small>
								</div>

								<div class='row'>
									<input type='text' name='slug' style='width:100%' value='<?php echo $inv_meta_box->slug; ?>' />
								</div>
							</td>
						</tr>

						<tr class='tr_description'>
							<td>
								<div class='row'>
									<label for='description'><strong>Description</strong></label>
								</div>

								<div class='row'>
									<small>A short descriptive summary of what the meta box is about.</small>
								</div>

								<div class='row'>
									<textarea name='description' style='width:100%' rows='2'><?php echo $inv_meta_box->description; ?></textarea>
								</div>
							</td>
						</tr>
					</tbody>
				</table>

				<div class='clear'></div>
			</div>

			<div id="postbox-container-1" class="postbox-container">
				<?php do_meta_boxes('', 'side', null); ?>
			</div>

			<div id='postbox-container-2' class='postbox-container'>
				<?php do_meta_boxes('', 'normal', null); ?>

				<?php do_meta_boxes('', 'advanced', null); ?>
			</div>

			<a class="button-primary" href="#" id="btn-add-cus-field-modal" data-title="Add Custom Fields" data-width="300" data-height="300" data-id="modal-add-cus-field">Add Custom Fields</a>
		</div>
	</div>
</form>

<div id="modal-add-cus-field" style="display:none">
	<?php include(INV_ADM_DIR . 'popup/add-cus-field.php'); ?>
</div>
