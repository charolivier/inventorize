<!--METABOX index.php-->

<ul class="subsubsub">
	<li class="all">
		<a href="<?php echo admin_url('admin.php'); ?>?page=inventorize-meta-box" class="current">
			<span>All </span>
			<span class="count">(<?php echo count($this->get_row($this->tab_meta_box)); ?>)</span>
		</a>
		<span> |</span>
	</li>
	<li class="publish">
		<a href="<?php echo admin_url('admin.php'); ?>?page=inventorize-meta-box&status=published">
			<span>Published </span>
			<?php $arr_published = $this->get_row($this->tab_meta_box, array('status'=>'published')); ?>

			<span class="count">(<?php echo is_array($arr_published) ? count($arr_published) : '0'; ?>)</span>
		</a>
	</li>
</ul>

<form id='inv-post-type' action='<?php echo admin_url('admin.php'); ?>'>
	<input type='hidden' name='page' value='inventorize-post-type'>

	<p class='search-box'>
		<label class='screen-reader-text' for='tag-search-input'>Search Inventory:</label>
		<input type='search' id='inv-search-input' name='s' value='<?php echo (empty($_GET['s'])) ? '' : attribute_escape(stripslashes($_GET['s'])); ?>'>
		<input type='submit' id='inv-search-submit' class='button' value='Search Inventory'>
	</p>

	<div class='tablenav top'>
		<div class='alignleft actions bulkactions'>
			<label for='inv-bulk-action-selector-top' class='screen-reader-text'>Select bulk action</label>

			<select name='action' id='inv-bulk-action-selector-top'>
				<option value='-1'>Bulk Actions</option>

				<option value='delete'>Delete</option>
			</select>

			<input type='submit' id='inv-doaction' class='button action' value='Apply'>
		</div>

		<br class='clear'>
	</div>

	<table class='wp-list-table widefat fixed striped tags'>
		<thead>
			<tr>
				<td id='cb' class='manage-column column-cb check-column'>
					<input id='cb-select-all-1' type='checkbox'>
				</td>

				<?php 
				$order = array('desc','asc'); 

				if(isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'name' && $_REQUEST['order']=='asc') {
					$order = array('asc','desc'); 
				}
				?>

				<th scope='col' id='name' class='manage-column column-name column-primary sortable <?php echo $order[0]; ?>'>
					<a href='<?php echo $this->url; ?>&action=index&orderby=name&order=<?php echo $order[1]; ?>'>
						<span>Name</span>

						<span class='sorting-indicator'></span>
					</a>
				</th>

				<?php $order = array('desc','asc'); 

				if(isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'slug' && $_REQUEST['order']=='asc') {
					$order = array('asc','desc');
				} 
				?>

				<th scope='col' id='slug' class='manage-column column-slug sortable <?php echo $order[0]; ?>'>
					<a href='<?php echo $this->url; ?>&action=index&orderby=slug&order=<?php echo $order[1]; ?>'>
						<span>Slug</span>

						<span class='sorting-indicator'></span>
					</a>
				</th>

				<th scope='col' id='description' class='manage-column column-description sortable desc'>
					<span>Description</span>
				</th>
			</tr>
		</thead>

		<tbody id='inv-list'>
			<?php 
			$arr = array();
			if(isset($_REQUEST['status']) && $_REQUEST['status'] === 'published') $arr['status'] = 'published';
			if(isset($_REQUEST['s'])) $arr['s'] = $_REQUEST['s'];
			$list = (empty($arr)) ? (array)$this->get_row($this->tab_meta_box) : (array)$this->get_row($this->tab_meta_box, $arr);
			$list = array_filter($list);
			?>

			<?php if(!empty($list)) { ?>
				<?php foreach($list as $k=>$v) { ?>
					<tr id='<?php echo $v->id; ?>'>
						<th scope='row' class='check-column'>
							<input type='checkbox' id='cb-select-<?php echo $v->id; ?>' name='id[]' value='<?php echo $v->id; ?>'>
						</th>

						<td class='name column-name has-row-actions column-primary' data-colname='Name'>
							<strong>
								<a class='row-name' href='<?php echo $this->url; ?>&action=edit&id=<?php echo $v->id; ?>' aria-label='“<?php echo $v->name; ?>” (Edit)'><?php echo $v->name; ?></a>
							</strong>

							<br/>

							<div class='row-actions'>
								<span class='edit'>
									<a href='<?php echo $this->url; ?>&action=edit&id=<?php echo $v->id; ?>' aria-label='Edit “<?php echo $v->name; ?>”'>Edit</a>
								</span>

								<span> | </span>

								<span class='view'>
									<a href='<?php echo $this->url; ?>' aria-label='View “<?php echo $v->name; ?>”'>View</a>
								</span>

								<span> | </span>

								<span class='delete'>
									<a href='<?php echo $this->url; ?>&action=delete&id=<?php echo $v->id; ?>' class='submitdelete' aria-label='Delete “<?php echo $v->name; ?>”' onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
								</span>
							</div>

							<button type='button' class='toggle-row'>
								<span class='screen-reader-text'>Show more details</span>
							</button>
						</td>

						<td class='slug column-slug' data-colname='Slug'><?php echo $v->slug; ?></td>

						<td class='description column-description' data-colname='Description'><?php echo $v->description; ?></td>
					</tr>
				<?php } ?>
			<?php } else { ?>
				<tr>
					<td colspan='4'>
						<span>No result</span>
					</td>
				</tr>
			<?php } ?>
		</tbody>

		<tfoot>
			<tr>
				<td class='manage-column column-cb check-column'>
					<input id='cb-select-all-2' type='checkbox'>
				</td>

				<th scope='col' class='manage-column column-name column-primary sortable desc'>
					<a href='<?php echo $this->url; ?>&action=index&taxonomy=category&orderby=name&order=asc'>
						<span>Name</span>

						<span class='sorting-indicator'></span>
					</a>
				</th>

				<th scope='col' class='manage-column column-slug sortable desc'>
					<a href='<?php echo $this->url; ?>&action=index&taxonomy=category&orderby=slug&order=asc'>
						<span>Slug</span>

						<span class='sorting-indicator'></span>
					</a>
				</th>

				<th scope='col' class='manage-column column-description sortable desc'>
					<a href='<?php echo $this->url; ?>&action=index&taxonomy=category&orderby=description&order=asc'>
						<span>Description</span>

						<span class='sorting-indicator'></span>
					</a>
				</th>
			</tr>
		</tfoot>
	</table>
</form>
