<!--TAXONOMY edit.php-->

<?php $inv_taxonomy = $this->get_taxonomy(); ?>

<form id='inv-taxonomy' action='<?php echo admin_url('admin.php'); ?>'>
	<input type='hidden' name='page' value='inventorize-taxonomy'>

	<input type='hidden' name='update' value='1'>

	<?php if($error !== 'create' && ($action && $action === 'edit' || isset($this->req['update']))) { ?>
		<input type='hidden' name='action' value='edit'>

		<?php wp_nonce_field('inv_taxonomy_edit','inv_taxonomy_edit_nonce_field',0); ?>

		<input type='hidden' name='id' value='<?php echo $inv_taxonomy->id; ?>'>
	<?php } else { ?>
		<input type='hidden' name='action' value='create'>

		<?php wp_nonce_field('inv_taxonomy_create','inv_taxonomy_create_nonce_field'); ?>
	<?php } ?>

	<?php wp_nonce_field('meta-box-order', 'meta-box-order-nonce', false ); ?>

	<?php wp_nonce_field('closedpostboxes', 'closedpostboxesnonce', false ); ?>

	<div id='poststuff'>
		<div id='post-body' class='metabox-holder columns-<?php echo 1 == get_current_screen()->get_columns() ? '1' : '2'; ?>'>
			<div id='post-body-content'>
				<table class='widefat fixed striped'>
					<thead>
						<tr>
							<th>Name and description</th>
						</tr>
					</thead>

					<tbody>
						<tr class='tr_name'>
							<td>
								<div class='row'>
									<label for='name'><strong>Name</strong></label>
								</div>

								<div class='row'>
									<small>General name</small>
								</div>

								<div class='row'>
									<input type='text' name='name' style='width:100%' value='<?php echo $inv_taxonomy->name; ?>' />
								</div>
							</td>
						</tr>

						<tr class='tr_name_plural'>
							<td>
								<div class='row'>
									<label for='name_plural'><strong>Name Plural</strong></label>
								</div>

								<div class='row'>
									<small>Name plural</small>
								</div>

								<div class='row'>
									<input type='text' name='name_plural' style='width:100%' value='<?php echo $inv_taxonomy->name_plural; ?>' />
								</div>
							</td>
						</tr>

						<tr class='tr_slug'>
							<td>
								<div class='row'>
									<label for='slug'><strong>Slug</strong></label>
								</div>

								<div class='row'>
									<small>User friendly and URL valid name</small>
								</div>

								<div class='row'>
									<input type='text' name='slug' style='width:100%' value='<?php echo $inv_taxonomy->slug; ?>' />
								</div>
							</td>
						</tr>

						<tr class='tr_description'>
							<td>
								<div class='row'>
									<label for='description'><strong>Description</strong></label>
								</div>

								<div class='row'>
									<small>A short descriptive summary of what the Inventory is about.</small>
								</div>

								<div class='row'>
									<textarea name='description' style='width:100%' rows='2'><?php echo $inv_taxonomy->description; ?></textarea>
								</div>
							</td>
						</tr>
					</tbody>
				</table>

				<div class='clear'></div>
			</div>

			<div id="postbox-container-1" class="postbox-container">
				<?php do_meta_boxes('', 'side', null); ?>
			</div>

			<div id='postbox-container-2' class='postbox-container'>
				<?php do_meta_boxes('', 'normal', null); ?>

				<?php do_meta_boxes('', 'advanced', null); ?>
			</div>
		</div>
	</div>
</form>
