<!--POST-TYPE edit.php-->

<?php $inv_post_type = $this->get_post_type(); ?>

<form id='inv-post-type' action='<?php echo admin_url('admin.php'); ?>'>
	<input type='hidden' name='page' value='inventorize-post-type'>

	<input type='hidden' name='update' value='1'>

	<?php if($error !== 'create' && ($action && $action === 'edit' || isset($this->req['update']))) { ?>
		<input type='hidden' name='action' value='edit'>

		<?php wp_nonce_field('inv_post_type_edit','inv_post_type_edit_nonce_field',0); ?>

		<input type='hidden' name='id' value='<?php echo $inv_post_type->id; ?>'>
	<?php } else { ?>
		<input type='hidden' name='action' value='create'>

		<?php wp_nonce_field('inv_post_type_create','inv_post_type_create_nonce_field'); ?>
	<?php } ?>

	<?php wp_nonce_field('meta-box-order', 'meta-box-order-nonce', false ); ?>

	<?php wp_nonce_field('closedpostboxes', 'closedpostboxesnonce', false ); ?>

	<div id='poststuff'>
		<div id='post-body' class='metabox-holder columns-<?php echo 1 == get_current_screen()->get_columns() ? '1' : '2'; ?>'>
			<div id='post-body-content'>
				<table class='widefat fixed striped'>
					<thead>
						<tr>
							<th>Name and description</th>
						</tr>
					</thead>

					<tbody>
						<tr class='tr_name'>
							<td>
								<div class='row'>
									<label for='name'><strong>Name</strong></label>
								</div>

								<div class='row'>
									<small>General name</small>
								</div>

								<div class='row'>
									<input type='text' name='name' style='width:100%' value='<?php echo $inv_post_type->name; ?>' />
								</div>
							</td>
						</tr>

						<tr class='tr_name_plural'>
							<td>
								<div class='row'>
									<label for='name_plural'><strong>Name Plural</strong></label>
								</div>

								<div class='row'>
									<small>Name plural</small>
								</div>

								<div class='row'>
									<input type='text' name='name_plural' style='width:100%' value='<?php echo $inv_post_type->name_plural; ?>' />
								</div>
							</td>
						</tr>

						<tr class='tr_slug'>
							<td>
								<div class='row'>
									<label for='slug'><strong>Slug</strong></label>
								</div>

								<div class='row'>
									<small>User friendly and URL valid name</small>
								</div>

								<div class='row'>
									<input type='text' name='slug' style='width:100%' value='<?php echo $inv_post_type->slug; ?>' />
								</div>
							</td>
						</tr>

						<tr class='tr_description'>
							<td>
								<div class='row'>
									<label for='description'><strong>Description</strong></label>
								</div>

								<div class='row'>
									<small>A short descriptive summary of what the Inventory is about.</small>
								</div>

								<div class='row'>
									<textarea name='description' style='width:100%' rows='2'><?php echo $inv_post_type->description; ?></textarea>
								</div>
							</td>
						</tr>

						<tr class='tr_menu_icon'>
							<td>
								<div class='row'>
									<label for='menu_icon'><strong>Menu Icon</strong></label>
								</div>

								<div class='row'>
									<?php 
									$inv_img_id = $inv_post_type->menu_icon;
									if($inv_img_id) $inv_img_src = wp_get_attachment_image_src($inv_img_id, 'favicon')[0];
									if(isset($inv_img_src)) $inv_img_arr = explode('/', $inv_img_src);
									?>

									<small>
										<span>Image to be used for this inventory</span>

										<strong id='menu_icon_path'><?php echo (isset($inv_img_src)) ? end($inv_img_arr) : ''; ?></strong>
									</small>
								</div>

								<div class='row'>
									<button class='button upload' type="button" style='margin: 5px 0'>upload icon</button>

									<input type='hidden' name='menu_icon' id='menu_icon' value='<?php echo $inv_img_id; ?>'>
								</div>
							</td>
						</tr>
					</tbody>
				</table>

				<div class='clear'></div>
			</div>

			<div id="postbox-container-1" class="postbox-container">
				<?php do_meta_boxes('', 'side', null); ?>
			</div>

			<div id='postbox-container-2' class='postbox-container'>
				<?php do_meta_boxes('', 'normal', null); ?>

				<?php do_meta_boxes('', 'advanced', null); ?>
			</div>
		</div>
	</div>
</form>
