<!--POST - CUS FIELD - TEXT INPUT-->

<div class='inv-cus-field-wrp clearfix'>
	<div class='row'>
		<label for='inv_<?php echo $data['field']->slug; ?>'>
			<strong><?php echo $data['field']->name; ?></strong>
		</label>
	</div>

	<div class='row'>
		<input name='inv_<?php echo $data['field']->slug; ?>' type='text' class='widefat' value='<?php echo get_post_meta($data['post']->ID, 'inv_'.$data['field']->slug, true); ?>'>
	</div>
</div>