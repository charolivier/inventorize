<!--TAXONOMY options.php-->

<table class='widefat fixed striped'>
	<tr class='tr_public'>
		<td>
			<div class='row'>
				<input type='checkbox' name='public' <?php echo ($inv_taxonomy->public)?'checked':'' ?> />

				<label for='public'><strong>Public</strong></label>
			</div>

			<div class='row'>
				<small>Controls how the type is visible to authors and readers. <strong>'checked'</strong> - Implies exclude_from_search: false, publicly_queryable: true, show_in_nav_menus: true, and show_ui:true. The built-in types attachment, page, and post are similar to this. <strong>'unchecked'</strong> - Implies exclude_from_search: true, publicly_queryable: false, show_in_nav_menus: false, and show_ui: false. The built-in types nav_menu_item and revision are similar to this. Best used if you'll provide your own editing and viewing interfaces (or none at all).</small>
			</div>
		</td>
	</tr>

	<tr class='tr_publicly_queryable'>
		<td>
			<div class='row'>
				<input type='checkbox' name='publicly_queryable' <?php echo ($inv_taxonomy->publicly_queryable)?'checked':'' ?> />

				<label for='publicly_queryable'><strong>Publicly Queryable</strong></label>
			</div>

			<div class='row'>
				<small>Whether queries can be performed on the front end as part of parse_request()</small>
			</div>
		</td>
	</tr>

	<tr class='tr_show_ui'>
		<td>
			<div class='row'>
				<input type='checkbox' name='show_ui' <?php echo ($inv_taxonomy->show_ui)?'checked':'' ?> />

				<label for='show_ui'><strong>Show UI</strong></label>
			</div>

			<div class='row'>
				<small>Whether to generate a default UI for managing this post type in the admin.</small>
			</div>
		</td>
	</tr>

	<tr class='tr_show_in_menu'>
		<td>
			<div class='row'>
				<input type='checkbox' name='show_in_menu' <?php echo ($inv_taxonomy->show_in_menu)?'checked':'' ?> />

				<label for='show_in_menu'><strong>Show in Menu</strong></label>
			</div>

			<div class='row'>
				<small>Where to show the post type in the admin menu. show_ui must be checked.</small>
			</div>
		</td>
	</tr>

	<tr class='tr_show_in_nav_menus'>
		<td>
			<div class='row'>
				<input type='checkbox' name='show_in_nav_menus' <?php echo ($inv_taxonomy->show_in_nav_menus)?'checked':'' ?> />

				<label for='show_in_nav_menus'><strong>Show in Nav Menus</strong></label>
			</div>

			<div class='row'>
				<small>Whether taxonomy is available for selection in navigation menus.</small>
			</div>
		</td>
	</tr>

	<tr class='tr_show_in_rest'>
		<td>
			<div class='row'>
				<input type='checkbox' name='show_in_rest' <?php echo ($inv_taxonomy->show_in_rest)?'checked':'' ?> />

				<label for='show_in_rest'><strong>Show in Rest</strong></label>
			</div>

			<div class='row'>
				<small>Whether to include the taxonomy in the REST API.</small>
			</div>
		</td>
	</tr>

	<tr class='tr_show_in_quick_edit'>
		<td>
			<div class='row'>
				<input type='checkbox' name='show_in_quick_edit' <?php echo ($inv_taxonomy->show_in_quick_edit)?'checked':'' ?> />

				<label for='show_in_quick_edit'><strong>Show in Quick Edit</strong></label>
			</div>

			<div class='row'>
				<small>Whether to show the taxonomy in the quick/bulk edit panel.</small>
			</div>
		</td>
	</tr>

	<tr class='tr_show_admin_column'>
		<td>
			<div class='row'>
				<input type='checkbox' name='show_admin_column' <?php echo ($inv_taxonomy->show_admin_column)?'checked':'' ?> />

				<label for='show_admin_column'><strong>Show Admin Column</strong></label>
			</div>

			<div class='row'>
				<small>Whether to allow automatic creation of taxonomy columns on associated post-types table.</small>
			</div>
		</td>
	</tr>

	<tr class='tr_hierarchical'>
		<td>
			<div class='row'>
				<input type='checkbox' name='hierarchical' <?php echo ($inv_taxonomy->hierarchical)?'checked':'' ?> />

				<label for='hierarchical'><strong>Show Admin Column</strong></label>
			</div>

			<div class='row'>
				<small>Is this taxonomy hierarchical (have descendants) like categories or not hierarchical like tags.</small>
			</div>
		</td>
	</tr>

	<tr class='tr_sort'>
		<td>
			<div class='row'>
				<input type='checkbox' name='sort' <?php echo ($inv_taxonomy->sort)?'checked':'' ?> />

				<label for='sort'><strong>Sort</strong></label>
			</div>

			<div class='row'>
				<small>Whether this taxonomy should remember the order in which terms are added to objects.</small>
			</div>
		</td>
	</tr>
</table>