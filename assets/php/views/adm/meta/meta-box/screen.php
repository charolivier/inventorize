<!--METABOX screen.php-->

<table class='widefat fixed striped'>
	<tr class='tr_screen'>
		<td>
			<div class='row'>
				<small>What inventory to include this Field Group</small>
			</div>

			<div class='row'>
				<?php $screen = json_decode($inv_meta_box->screen); ?>

				<?php if(!empty($this->get_row($this->tab_post_type))) { ?>
					<select class='chosen-select' name='screen[]' multiple>
						<?php foreach($this->get_row($this->tab_post_type) as $k=>$v) { ?>
							<option <?php echo (in_array($v->slug, $screen))?'selected':''; ?> value='<?php echo $v->slug; ?>'><?php echo $v->name; ?></option>
						<?php } ?>
					</select>
				<?php } else { ?>
					<a href='<?php echo $this->url; ?>/'>Create new inventory to enable this option</a>
				<?php } ?>
			</div>
		</td>
	</tr>
</table>