<!--META BOX - TEXT INPUT DEF-->

<?php 
if(isset($data)) {
	$data = array(
		'id'=>$data->id,
		'name'=>isset($data->name) ? $data->name : '',
		'slug'=>isset($data->slug) ? $data->slug : '',
		'desc'=>isset($data->desc) ? $data->desc : ''		
	);
} else {
	$data = array(
		'id'=>$field->id,
		'name'=>'',
		'slug'=>'',
		'desc'=>''
	);
}

$content = "
	<div id='inv-field-".$data['id']."' class='postbox postbox-field'>
		<input type='hidden' name='fields[".$data['id']."][id]' value='".$data['id']."' />

		<input type='hidden' name='fields[".$data['id']."][type]' value='text-input' />
		
		<input type='hidden' name='fields[".$data['id']."][temp]' value='0' />

		<button type='button' class='handlediv' aria-expanded='true'>
			<span class='screen-reader-text'>Toggle panel: text input</span>

			<span class='toggle-indicator' aria-hidden='true'></span>
		</button>

		<h2 class='hndle ui-sortable-handle'>
			<span>
				<bold>".$data['name']."</bold>

				<small>text input</small>
			</span>
		</h2>

		<div class='inside'>
			<table class='widefat fixed striped'>
				<tbody>
					<tr class='tr_name'>
						<td>
							<div class='row'>
								<strong>name</strong>
							</div>
							<div class='row'>
								<input class='widefat' type='text' name='fields[".$data['id']."][name]' value='".$data['name']."' />
							</div>
						</td>
					</tr>
					<tr class='tr_slug'>
						<td>
							<div class='row'>
								<strong>slug</strong>
							</div>
							<div class='row'>
								<input class='widefat' type='text' name='fields[".$data['id']."][slug]' value='".$data['slug']."' />
							</div>
						</td>
					</tr>
					<tr class='tr_description'>
						<td>
							<div class='row'>
								<strong>description</strong>
							</div>
							<div class='row'>
								<textarea name='fields[".$data['id']."][description]' style='width:100%' rows='2' />".$data['desc']."</textarea>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
"; ?>