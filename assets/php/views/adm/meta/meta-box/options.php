<!--METABOX options.php-->

<table class='widefat fixed striped'>
	<tr class='tr_context'>
		<td>
			<div class='row'>
				<small>Its used to provide the position of the custom meta on the display screen.</small>
			</div>

			<div class='row'>
				<?php $inv_context = $inv_meta_box->context; ?>

				<select class='widefat' name='context'>
					<option <?php echo ($inv_context==='normal')?'selected':''; ?> value='normal'>Normal</option>
					<option <?php echo ($inv_context==='advanced')?'selected':''; ?> value='advanced'>Advanced</option>
					<option <?php echo ($inv_context==='side')?'selected':''; ?> value='side'>Side</option>
				</select>
			</div>
		</td>
	</tr>

	<tr class='tr_priority'>
		<td>
			<div class='row'>
				<small>Used to provide the position of the box in the provided context.</small>
			</div>

			<div class='row'>
				<?php $inv_priority = $inv_meta_box->priority; ?>

				<select class='widefat' name='priority'>
					<option <?php echo ($inv_priority==='high')?'selected':''; ?> value='high'>High</option>
					<option <?php echo ($inv_priority==='core')?'selected':''; ?> value='core'>Core</option>
					<option <?php echo ($inv_priority==='default')?'selected':''; ?> value='default'>Default</option>
					<option <?php echo ($inv_priority==='low')?'selected':''; ?> value='low'>Low</option>
				</select>
			</div>
		</td>
	</tr>
</table>