<!--METABOX submitdiv.php-->

<div class="submitbox" id="submitpost">
	<div class="misc-pub-section">
		<div class='row'>
			<small for='menu_position'>Status</small>
		</div>

		<div class='row'>
			<select name="status" id="status" class="widefat">
				<option value="published" <?php echo ($inv_meta_box->status=='published' )?'selected':''; ?>>Published</option>
				<option value="draft" <?php echo ($inv_meta_box->status=='draft' )?'selected':''; ?>>Draft</option>
			</select>
		</div>
	</div>

	<div id="major-publishing-actions">
		<div id="delete-action">
			<a class="submitdelete deletion" href="<?php echo $this->url; ?>&action=delete&id=<?php echo $inv_meta_box->id; ?>">Move to Trash</a>
		</div>

		<div id="publishing-action">
			<span class="spinner"></span>

			<input name="original_publish" type="hidden" id="original_publish" value="Publish" />

			<input type="submit" name="publish" id="publish" class="button button-primary button-large" value="Publish" />
		</div>

		<div class="clear"></div>
	</div>
</div>