<!--POST-TYPE support.php-->

<table class='widefat fixed striped'>
	<tbody>
		<tr class='tr_supports'>
			<td>
				<div class='row'>
					<label for='supports'><strong>Supports</strong></label>
				</div>

				<div class='row'>
					<small>Post type's support for a given feature.</small>
				</div>

				<div class='row'>
					<?php $inv_supports = json_decode($inv_post_type->supports); ?>

					<select name='supports[]' multiple='multiple' class='chosen-select'>
						<option <?php echo in_array('title', $inv_supports)?'selected':''; ?> value='title'>title</option>
						<option <?php echo in_array('editor', $inv_supports)?'selected':''; ?> value='editor'>editor</option>
						<option <?php echo in_array('author', $inv_supports)?'selected':''; ?> value='author'>author</option>
						<option <?php echo in_array('thumbnail', $inv_supports)?'selected':''; ?> value='thumbnail'>thumbnail</option>
						<option <?php echo in_array('excerpt', $inv_supports)?'selected':''; ?> value='excerpt'>excerpt</option>
						<option <?php echo in_array('trackbacks', $inv_supports)?'selected':''; ?> value='trackbacks'>trackbacks</option>
						<option <?php echo in_array('custom-fields', $inv_supports)?'selected':''; ?> value='custom-fields'>custom-fields</option>
						<option <?php echo in_array('comments', $inv_supports)?'selected':''; ?> value='comments'>comments</option>
						<option <?php echo in_array('revisions', $inv_supports)?'selected':''; ?> value='revisions'>revisions</option>
						<option <?php echo in_array('page-attributes', $inv_supports)?'selected':''; ?> value='page-attributes'>page-attributes</option>
						<option <?php echo in_array('post-formats', $inv_supports)?'selected':''; ?> value='post-formats'>post-formats</option>
					</select>
				</div>
			</td>
		</tr>
	</tbody>
</table>