<!--POST-TYPE metabox.php-->

<table class='widefat fixed striped'>
	<tbody>
		<tr class='tr_taxonomies'>
			<td>
				<div class='row'>
					<label for='taxonomies'><strong>Taxonomies</strong></label>
				</div>

				<div class='row'>
					<small>Taxonomy supported by the inventory.</small>
				</div>

				<div class='row'>
					<?php $inv_meta_box = json_decode($inv_post_type->meta_box); ?>

					<select name='meta_box[]' multiple='multiple' class='chosen-select'>
						<?php foreach($this->get_row($this->tab_meta_box) as $k=>$v) { ?>
							<option <?php echo (in_array($v->slug, $inv_meta_box))?'selected':''; ?> value='<?php echo $v->slug; ?>'><?php echo $v->name; ?></option>
						<? } ?>
					</select>
				</div>
			</td>
		</tr>
	</tbody>
</table>