<!--POST-TYPE submitdiv.php-->

<div class="submitbox" id="submitpost">
	<div class="misc-pub-section">
		<div class='row'>
			<small for='menu_position'>Status</small>
		</div>

		<div class='row'>
			<select name="status" id="status" class="widefat">
				<option value="published" <?php echo ($inv_post_type->status=='published' )?'selected':''; ?>>Published</option>
				<option value="draft" <?php echo ($inv_post_type->status=='draft' )?'selected':''; ?>>Draft</option>
			</select>
		</div>
	</div>

	<?php if($inv_post_type->show_in_menu) { ?>
		<div class="misc-pub-section">
			<div class='row'>
				<small for='menu_position'>Menu Position (below)</small>
			</div>

			<div class='row'>
				<select name='menu_position' class='widefat'>
					<option <?php echo ($inv_post_type->menu_position==5  )?'selected':''; ?> value='5'>Posts</option>
					<option <?php echo ($inv_post_type->menu_position==10 )?'selected':''; ?> value='10'>Media</option>
					<option <?php echo ($inv_post_type->menu_position==15 )?'selected':''; ?> value='15'>Links</option>
					<option <?php echo ($inv_post_type->menu_position==20 )?'selected':''; ?> value='20'>Pages</option>
					<option <?php echo ($inv_post_type->menu_position==25 )?'selected':''; ?> value='25'>Comments</option>
					<option <?php echo ($inv_post_type->menu_position==60 )?'selected':''; ?> value='60'>First separator</option>
					<option <?php echo ($inv_post_type->menu_position==65 )?'selected':''; ?> value='65'>Plugins</option>
					<option <?php echo ($inv_post_type->menu_position==70 )?'selected':''; ?> value='70'>Users</option>
					<option <?php echo ($inv_post_type->menu_position==75 )?'selected':''; ?> value='75'>ools</option>
					<option <?php echo ($inv_post_type->menu_position==80 )?'selected':''; ?> value='80'>Settings</option>
					<option <?php echo ($inv_post_type->menu_position==100)?'selected':''; ?> value='100'>Second separator</option>
				</select>
			</div>
		</div>
	<?php } ?>

	<div id="major-publishing-actions">
		<div id="delete-action">
			<a class="submitdelete deletion" href="<?php echo $this->url; ?>&action=delete&id=<?php echo $inv_post_type->id; ?>">Move to Trash</a>
		</div>

		<div id="publishing-action">
			<span class="spinner"></span>

			<input name="original_publish" type="hidden" id="original_publish" value="Publish" />

			<input type="submit" name="publish" id="publish" class="button button-primary button-large" value="Publish" />
		</div>

		<div class="clear"></div>
	</div>
</div>