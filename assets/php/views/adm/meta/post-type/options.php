<!--POST-TYPE options.php-->

<table class='widefat fixed striped'>
	<tr class='tr_public'>
		<td>
			<div class='row'>
				<input type='checkbox' name='public' <?php echo ($inv_post_type->public)?'checked':'' ?> />

				<label for='public'><strong>Public</strong></label>
			</div>

			<div class='row'>
				<small>Controls how the type is visible to authors and readers. <strong>'checked'</strong> - Implies exclude_from_search: false, publicly_queryable: true, show_in_nav_menus: true, and show_ui:true. The built-in types attachment, page, and post are similar to this. <strong>'unchecked'</strong> - Implies exclude_from_search: true, publicly_queryable: false, show_in_nav_menus: false, and show_ui: false. The built-in types nav_menu_item and revision are similar to this. Best used if you'll provide your own editing and viewing interfaces (or none at all).</small>
			</div>
		</td>
	</tr>

	<tr class='tr_publicly_queryable'>
		<td>
			<div class='row'>
				<input type='checkbox' name='publicly_queryable' <?php echo ($inv_post_type->publicly_queryable)?'checked':'' ?> />

				<label for='publicly_queryable'><strong>Publicly Queryable</strong></label>
			</div>

			<div class='row'>
				<small>Whether queries can be performed on the front end as part of parse_request()</small>
			</div>
		</td>
	</tr>

	<tr class='tr_show_ui'>
		<td>
			<div class='row'>
				<input type='checkbox' name='show_ui' <?php echo ($inv_post_type->show_ui)?'checked':'' ?> />

				<label for='show_ui'><strong>Show UI</strong></label>
			</div>

			<div class='row'>
				<small>Whether to generate a default UI for managing this post type in the admin.</small>
			</div>
		</td>
	</tr>

	<tr class='tr_show_in_nav_menus'>
		<td>
			<div class='row'>
				<input type='checkbox' name='show_in_nav_menus' <?php echo ($inv_post_type->show_in_nav_menus)?'checked':'' ?> />

				<label for='show_in_nav_menus'><strong>Show in Nav Menus</strong></label>
			</div>

			<div class='row'>
				<small>Whether post_type is available for selection in navigation menus.</small>
			</div>
		</td>
	</tr>

	<tr class='tr_show_in_menu'>
		<td>
			<div class='row'>
				<input type='checkbox' name='show_in_menu' <?php echo ($inv_post_type->show_in_menu)?'checked':'' ?> />

				<label for='show_in_menu'><strong>Show in Menu</strong></label>
			</div>

			<div class='row'>
				<small>Where to show the post type in the admin menu. show_ui must be checked.</small>
			</div>
		</td>
	</tr>

	<tr class='tr_has_archive'>
		<td>
			<div class='row'>
				<input type='checkbox' name='has_archive' <?php echo ($inv_post_type->has_archive)?'checked':'' ?> />

				<label for='show_in_nav_menus'><strong>Has archive</strong></label>
			</div>

			<div class='row'>
				<small>Enables post type archives. Will use $post_type as archive slug by default.</small>
			</div>
		</td>
	</tr>

	<tr class='tr_show_in_admin_bar'>
		<td>
			<div class='row'>
				<input type='checkbox' name='show_in_admin_bar' <?php echo ($inv_post_type->show_in_admin_bar)?'checked':'' ?> />

				<label for='show_in_admin_bar'><strong>Show in Admin Bar</strong></label>
			</div>

			<div class='row'>
				<small>Whether to make this post type available in the WordPress admin bar.</small>
			</div>
		</td>
	</tr>

	<tr class='tr_hierarchical'>
		<td>
			<div class='row'>
				<input type='checkbox' name='hierarchical' <?php echo ($inv_post_type->hierarchical)?'checked':'' ?> />

				<label for='show_in_nav_menus'><strong>Hierarchical</strong></label>
			</div>

			<div class='row'>
				<small>Whether the post type is hierarchical (e.g. page). Allows Parent to be specified. The 'supports' parameter should contain 'page-attributes' to show the parent select box on the editor page.</small>
			</div>
		</td>
	</tr>
</table>