<!--POST-TYPE taxonomies.php-->

<table class='widefat fixed striped'>
	<tbody>
		<tr class='tr_taxonomies'>
			<td>
				<div class='row'>
					<label for='taxonomies'><strong>Taxonomies</strong></label>
				</div>

				<div class='row'>
					<small>Taxonomy supported by the inventory.</small>
				</div>

				<div class='row'>
					<?php $inv_taxonomies = json_decode($inv_post_type->taxonomies); ?>

					<select name='taxonomies[]' multiple='multiple' class='chosen-select'>
						<option <?php echo (in_array('category', $inv_taxonomies))?'selected':''; ?> value='category'>Category</option>
						<option <?php echo (in_array('post_tag', $inv_taxonomies))?'selected':''; ?> value='post_tag'>Tags</option>

						<?php foreach($this->get_row($this->tab_taxonomy) as $k=>$v) { ?>
							<option <?php echo (in_array($v->slug, $inv_taxonomies))?'selected':''; ?> value='<?php echo $v->slug; ?>'><?php echo $v->name; ?></option>
						<? } ?>
					</select>
				</div>
			</td>
		</tr>
	</tbody>
</table>