<div class='wrap'>
	<h2>Dashboard</h2>

	<br class='clear' />

	<form id='inv-post-type-new' action='<?php echo admin_url('admin.php'); ?>'>
		<input type='hidden' name='page' value='inventorize-post-type'>
		<input type='submit' value='Add New Inventory' class='button button-primary button-large'>
	</form>
</div>