<?php 
check_ajax_referer(INV_PLG_NAME . '_ajax_nonce', 'nonce');

if(isset($_POST['method']) && $_POST['method']==='new-cus-field'){
	if(isset($_POST['type'])) {
		$this->upd_custom_field_def();
		
		$fields = $this->get_row($this->tab_cus_field);

		$field = end($fields);
		
		include(INV_ADM_DIR . 'meta/meta-box/cus-field/'.$_POST['type'].'.php');

		echo $content;
	}
}

if(isset($_POST['method']) && $_POST['method']==='get-cus-field'){
	if(isset($_POST['id']))	{
		$metabox = $this->get_row($this->tab_meta_box, array('id'=>$_POST['id']));
		
		if(!empty($metabox)) {
			$fields = $metabox[0]->callback_args;

			foreach(json_decode($fields) as $v) {
				$data = $this->get_row($this->tab_cus_field, array('id'=>$v));

				if(!empty($data)) {
					$data = $data[0];

					include(INV_ADM_DIR . 'meta/meta-box/cus-field/'.$data->type.'.php');

					echo $content;
				}
			}
		}
	}
}
?>