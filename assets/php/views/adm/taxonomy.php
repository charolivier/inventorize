<?php 
$head  = "<strong>Taxonomies</strong><a href='".$this->url."&action=create' class='add-new-h2'>Add New</a>"; 

$temp = "index";

$error = $this->get_error();

$action = isset($this->req['action']) ? $this->req['action'] : 0;

if($action && in_array($action, array('edit','create'))) {
	$temp = 'edit';

	if($error !== 'create' && ($action === 'edit' || isset($this->req['update']))) {
		$head = "<strong>Edit Taxonomy</strong>";
	} else {
		$head = "<strong>Create new Taxonomy</strong>";
	}
}
?>

<div class='wrap'>
	<h2><?php echo $head; ?></h2>

	<?php if(isset($_SESSION['inv_notify'])) { ?>
		<div class="notice notice-<?php echo $_SESSION['inv_notify']['status']; ?> is-dismissible">
			<p><?php echo $_SESSION['inv_notify']['msg']?></p>
		</div>
	<?php } ?>

	<?php include('temp/taxonomy/'.$temp.'.php'); ?>
</div>

<?php if(array_key_exists('msg', $this->req)) unset($_SESSION['inv_notify']); ?>