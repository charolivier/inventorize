<?php
/*
  Plugin Name: Inventorize
  Plugin URI: https://wordpress.org/plugins/inventorize/
  Description: WordPress inventory Manager
  Version: 0.0.1
  Author: Charles Olivier
  Author URI: http://www.miraculouscode.com
  License: Modified BSD
*/

if(!defined('ABSPATH')) exit;

define('INV_PLG_DIR', dirname(__FILE__));
define('INV_PLG_FILE', __FILE__);

require_once('assets/php/logic/config.php');
require_once('assets/php/logic/class.php');
require_once('assets/php/logic/settings.php');
?>